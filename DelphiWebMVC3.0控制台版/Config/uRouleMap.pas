unit uRouleMap;

interface

uses
  MVC.Roule;

type
  TRouleMap = class(TRoule)
  public
    constructor Create(); override;
  end;

implementation

uses
  IndexController, MainController, RoleController, UserController, VIPController,
  PayController;

constructor TRouleMap.Create;
begin
  inherited;
  //·��,������,��ͼĿ¼
  SetRoule('', TIndexController, '', False);
  SetRoule('Main', TMainController, '');
  SetRoule('User', TUserController, 'user');
  SetRoule('Role', TRoleController, 'role');
  SetRoule('VIP', TVIPController, 'vip');
  SetRoule('Pay', TPayController, 'pay');

end;

end.

