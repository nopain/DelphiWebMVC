unit IndexController;

interface

uses
  System.SysUtils, System.Classes, XSuperObject, MVC.BaseController, MVC.Command;

type
  TIndexController = class(TBaseController)
    procedure home(value1, value2, value3, value4, value5: string);
    procedure Index;
    procedure login;
    procedure check;
    procedure verifycode;
    procedure setdata;
    procedure showxml;
    procedure showxml1;
  end;

implementation

uses
  UsersService, UsersInterface, uTableMap, XSuperJSON;


{ TIndexController }
procedure TIndexController.home(value1, value2, value3, value4, value5: string);
var
  s: string;
begin
//http://localhost:8004/home/ddd/12/32/eee/333.html
//http://localhost:8004/home/ddd/12/32/eee/333
//http://localhost:8004/home/ddd/12/32/eee/333?name=admin
 //α��̬��Rest���
  with view do
  begin
    s := InputByIndex(2);
    s := Input('name');
    ShowText(s + ' ' + value1 + ' ' + value2 + ' ' + value3 + ' ' + value4 + ' ' + value5);
  end;
end;

procedure TIndexController.check;
var
  s: string;
  map, ret: ISuperObject;
  code: string;
  user_service: IUsersInterface;
begin
  user_service := TUsersService.Create(View.Db);
  with view do
  begin
    map := SO();

    map.S['username'] := Input('username');
    map.S['pwd'] := Input('pwd');
    s := JsonToString(map.AsJSON());

    begin

      ret := user_service.checkuser(map);
      if ret <> nil then
      begin
        SessionSet('user', ret.AsJSON());
        Success(0, '��¼�ɹ�');
      end
      else
      begin
        Fail(-1, '��¼ʧ��,�����û�������');
      end;
    end;
//    else
//    begin
//      Fail(-1, '��֤�����');
//    end;
  end;
end;

procedure TIndexController.Index;
var
  s: string;
  jo1, jo: ISuperObject;
begin
  with View do
  begin
    s := Input('name');
//    jo:=so;
//    jo.S['name']:='���';
//    RedisSetKeyJSON('name',jo);
//    jo1:=RedisGetKeyJSON('name');
//    s:=jo1.AsJSON(False);
    SessionSet('nnn', 'sf');
    s := SessionGet('nnn');
    SessionRemove('nnn');
    ShowHTML('login');
  end;
end;

procedure TIndexController.login;
var
  s: string;
begin
  with View do
  begin

    SessionRemove('user');
    ShowHTML('login');
  end;
end;

procedure TIndexController.setdata;
var
  s: string;
begin
  s := Request.Content;
end;

procedure TIndexController.verifycode;
begin

end;

procedure TIndexController.showxml;
begin
  with view do
  begin

    ShowJSON(db.Default.Find(tb_users, ''));
  end;
end;

procedure TIndexController.showxml1;
begin
  with view do
  begin

    ShowJSON(db.Default.Find(tb_users, ''));
  end;
end;

end.

