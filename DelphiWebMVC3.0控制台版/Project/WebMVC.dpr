{*******************************************************}
{  注意:在Linux下调试时内需延时启动，同时拷贝资源       }
{       苏兴迎                                          }
{                                                       }
{       管理权限启动delphi ，部署exe管理员启动          }
{       EMail:pearroom@yeah.net                         }
{                                                       }
{*******************************************************}

program WebMVC;
{$APPTYPE CONSOLE}

uses
  MVC.Command,
  MVC.Config,
  uDBConfig in '..\Config\uDBConfig.pas',
  uGlobal in '..\Config\uGlobal.pas',
  uPlugin in '..\Config\uPlugin.pas',
  uRouleMap in '..\Config\uRouleMap.pas',
  uTableMap in '..\Config\uTableMap.pas',
  uInterceptor in '..\Config\uInterceptor.pas',
  RoleService in '..\Service\RoleService.pas',
  UsersService in '..\Service\UsersService.pas',
  RoleInterface in '..\Service\Interface\RoleInterface.pas',
  UsersInterface in '..\Service\Interface\UsersInterface.pas',
  IndexController in '..\Controller\IndexController.pas',
  MainController in '..\Controller\MainController.pas',
  PayController in '..\Controller\PayController.pas',
  RoleController in '..\Controller\RoleController.pas',
  UserController in '..\Controller\UserController.pas',
  VIPController in '..\Controller\VIPController.pas';

{$R *.res}

begin
  Config.password_key := '';   //配置文件解密秘钥
  _MVCFun.Run();

end.

