unit LoginController;

interface

uses
  System.SysUtils, System.Classes, xsuperobject, MVC.BaseController,
  Vcl.Imaging.jpeg, Vcl.Graphics;

type
  TLoginController = class(TBaseController)
  private
    function ShowVerifyCode(num: string): string;
    procedure verifycode;
  public
    procedure index();
    procedure check();
    procedure checknum();
    procedure getalldata();
    procedure getxml;
    procedure home(value1, value2, value3, value4, value5: string);
  end;

implementation

uses
  uTableMap, UsersService, UsersInterface;

procedure TLoginController.verifycode;
var
  code, s: string;
  i: integer;
const
  str = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
begin

  with view do
  begin
    for i := 0 to 3 do
    begin
      code := code + Copy(str, Random(Length(str)), 1);
    end;
    SessionSet('vcode', code);
    if Length(code) <> 4 then
    begin
      ShowText('error');
    end
    else
    begin
      s := ShowVerifyCode(code);
      Response.Content := 'data:image/jpeg;base64,' + s;
    end;
  end;
end;

procedure TLoginController.check();
var
  json: string;
  sdata, ret, wh: ISuperObject;
  username, pwd: string;
  sql: string;
  users_service: IUsersInterface;
  checknum: string;
begin
  ret := SO();

  with View do
  begin
    users_service := TUsersService.Create(Db);
    try
      username := Input('username');
      pwd := Input('pwd');
      checknum := Input('checknum');
      wh := SO();
      wh.S['username'] := username;
      wh.S['pwd'] := pwd;
      if checknum.ToLower = SessionGet('vcode').ToLower then
      begin
        sdata := users_service.checkuser(wh);
        if (sdata <> nil) then
        begin
          json := sdata.AsJSON;
          Sessionset('username', username);
          Sessionset('name', sdata.S['name']);
          SessionSetJSON('user', sdata);
          json := Sessionget('user');
          ret.I['code'] := 0;
          ret.S['message'] := '登录成功';
        end
        else
        begin
          ret.I['code'] := -1;
          ret.S['message'] := '登录失败';
        end;
      end
      else
      begin
        ret.I['code'] := -1;
        ret.S['message'] := '验证码错误';
      end;
      ShowJson(ret);
    except
      on e: Exception do
        ShowText(e.ToString);
    end;
  end;
end;

procedure TLoginController.checknum;
var
  code, s: string;
  i: integer;
const
  str = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
begin

  with view do
  begin
    for i := 0 to 3 do
    begin
      code := code + Copy(str, Random(Length(str)), 1);
    end;
    SessionSet('vcode', code);
    if Length(code) <> 4 then
    begin
      ShowText('error');
    end
    else
    begin
      s := ShowVerifyCode(code);
      Response.Content := 'data:image/jpeg;base64,' + s;
    end;
  end;
end;

function TLoginController.ShowVerifyCode(num: string): string;
var
  bmp_t: TBitmap;
  i: integer;
  s: string;
begin
  bmp_t := TBitmap.Create;
  try
    bmp_t.SetSize(90, 35);
    bmp_t.Transparent := True;
    for i := 1 to length(num) do
    begin
      s := num[i];
      bmp_t.Canvas.Rectangle(0, 0, 90, 35);
      bmp_t.Canvas.Pen.Style := psClear;
      bmp_t.Canvas.Brush.Style := bsClear;
      bmp_t.Canvas.Font.Color := Random(256) and $C0; // 新建个水印字体颜色
      bmp_t.Canvas.Font.Size := Random(6) + 11;
      bmp_t.Canvas.Font.Style := [fsBold];
      bmp_t.Canvas.Font.Name := 'Verdana';
      bmp_t.Canvas.TextOut(i * 15, 5, s); // 加入文字
    end;
    s := view.Plugin.Tool.BitmapToString(bmp_t);
    Result := s;
  finally
    FreeAndNil(bmp_t);
  end;
end;

procedure TLoginController.getalldata;
var
  ret: ISuperObject;
  users_service: IUsersInterface;
begin
  users_service := TUsersService.Create(View.Db);
  ret := users_service.getAlldata(nil);
  view.ShowJSON(ret);
end;

procedure TLoginController.getxml;
begin

end;

procedure TLoginController.home(value1, value2, value3, value4, value5: string);
var
  s: string;
begin
//http://localhost:8004/home/ddd/12/32/eee/333.html
//http://localhost:8004/home/ddd/12/32/eee/333
//http://localhost:8004/home/ddd/12/32/eee/333?name=admin
 //伪静态及Rest风格
  with view do
  begin
    s := InputByIndex(2);
    s := Input('name');
    ShowText(s + ' ' + value1 + ' ' + value2 + ' ' + value3 + ' ' + value4 + ' ' + value5);
  end;
end;

procedure TLoginController.index();
var
  ret: boolean;
  jo: ISuperObject;
  users_service: IUsersInterface;
  s: string;
begin

  if isGET then
    with View do
    begin
      RedisSetKeyText('name', 'hello');
      s := RedisGetKeyText('name');
      users_service := TUsersService.Create(Db);
      jo := SO();
      jo.S['id'] := '1212';
      users_service.check(jo);

      SessionRemove('username');
      ShowHTML('Login');
    end;
end;

end.

