unit IndexController;

interface

uses
  System.SysUtils, System.Classes, xsuperobject, MVC.BaseController,
  Vcl.Imaging.jpeg, Vcl.Graphics;

type
  TIndexController = class(TBaseController)
  public
    procedure Index();
  end;

implementation

{ TIndexController }

procedure TIndexController.Index;
var
  s: string;
begin
  with View do
  begin
    RedisSetKeyText('name', 'hello');
    s := RedisGetKeyText('name');
    Redirect('login', 'index');
  end;
end;

end.

