unit UsersController;

interface

uses
  System.SysUtils, System.Classes, xsuperobject, MVC.BaseController;

type
  TUsersController = class(TBaseController)
  public
    procedure Index();
    procedure Add();
    procedure Edit();
    procedure Savedata();
    procedure getList();
  end;

implementation

{ TUsersController }

uses
  uTableMap;

procedure TUsersController.Add;
begin
  with View do
  begin
    ShowHTML('add');
  end;
end;

procedure TUsersController.Edit;
var
  user: ISuperObject;
begin
  with View do
  begin
    user := Db.Default.FindFirst(tb_users, 'and id=' + Input('id'));
    setAttr('user', user.AsJSON());

    ShowHTML('edit');
  end;
end;

procedure TUsersController.getList;
var
  ret, sdata: ISuperObject;
  con: integer;
  pageindex, pagesize: integer;
  s: string;
begin

  ret := SO();
  with View do
  begin
    pageindex := StrToInt(Input('pageindex'));
    pagesize := StrToInt(Input('pagesize'));

    sdata := Db.Default.FindPage(con, tb_users, 'id', pageindex, pagesize);
    ret.A['rows']:= sdata.AsArray;
    ret.I['total'] := con;

    ShowJson(ret);
  end;
end;

procedure TUsersController.Index;
var
  sdata: ISuperObject;
begin
  with View do
  begin
    sdata := Db.Default.Find(tb_users, 'limit 10');
    setAttr('sdata', sdata.AsJSON());
    ShowHTML('index');
  end;
end;

procedure TUsersController.Savedata;
var
  userid: string;
  ret: ISuperObject;
begin
  ret := SO();
  with View do
  begin
    userid := Input('id');
    if (userid = '') then
      db.Default.dataset := Db.Default.AddData(tb_users)
    else
      db.Default.dataset := Db.Default.EditData(tb_users, 'id', userid);
    try
      with db.Default.dataset do
      begin

        FieldByName('name').AsString := Input('name');

        FieldByName('username').AsString := Input('username');

        FieldByName('phone').AsString := Input('phone');
        FieldByName('age').AsString := Input('age');
        FieldByName('sex').AsString := Input('sex');
        FieldByName('address').AsString := Input('address');
        FieldByName('idcard').AsString := Input('idcard');
        FieldByName('phone').AsString := Input('phone');
        Post;
      end;
      ret.I['code'] := 0;
      ret.S['message'] := '����ɹ�';
    except
      ret.I['code'] := -1;
      ret.S['message'] := '����ʧ��';
    end;
    ShowJson(ret);
  end;
end;

end.

