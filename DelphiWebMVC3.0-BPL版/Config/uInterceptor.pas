unit uInterceptor;

interface

uses
  System.SysUtils, MVC.View, System.Classes, MVC.BaseInterceptor;

type
  TInterceptor = class(TBaseInterceptor)
    function execute(View: TView; error: Boolean): Boolean;
  end;

implementation

{ TInterceptor }

function TInterceptor.execute(View: TView; error: Boolean): Boolean;
var
  url: string;
begin
  Result := false;
  with View do
  begin
    if (error) then
    begin
      Result := true;
      exit;
    end;
    url := LowerCase(Request.PathInfo);
    begin
      if (SessionGet('username') = '') then
      begin
        Result := true;
        Response.Content := '<script>window.location.href=''/'';</script>';
        Response.SendResponse;
      end;
    end;
  end;
end;

end.

