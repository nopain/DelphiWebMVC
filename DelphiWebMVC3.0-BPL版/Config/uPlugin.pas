unit uPlugin;

interface

uses
  Plugin.Tool;

type
  TPlugin = class
  public
    Tool:TTool;
    constructor Create;
    destructor Destroy; override;
  end;

implementation

{ TPlugin }

constructor TPlugin.Create;
begin

  Tool:=TTool.Create;
end;

destructor TPlugin.Destroy;
begin
  Tool.Free;
  inherited;
end;

end.

