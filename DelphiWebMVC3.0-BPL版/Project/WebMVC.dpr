{*******************************************************}
{                                                       }
{       苏兴迎                                          }
{       E-Mail:pearroom@yeah.net                        }
{       管理员权限启动delphi,管理员权限启动部署程序     }
{                                                       }
{*******************************************************}

program WebMVC;
{$APPTYPE GUI}

uses
  Vcl.Forms,
  MVC.Command,
  MVC.Main,
  MVC.Config,
  uDBConfig in '..\Config\uDBConfig.pas',
  uGlobal in '..\Config\uGlobal.pas',
  uPlugin in '..\Config\uPlugin.pas',
  uRouleMap in '..\Config\uRouleMap.pas',
  uTableMap in '..\Config\uTableMap.pas',
  uInterceptor in '..\Config\uInterceptor.pas',
  UsersInterface in '..\Service\Interface\UsersInterface.pas',
  UsersService in '..\Service\UsersService.pas',
  CaiWuController in '..\Controller\CaiWuController.pas',
  FirstController in '..\Controller\FirstController.pas',
  IndexController in '..\Controller\IndexController.pas',
  KuCunController in '..\Controller\KuCunController.pas',
  LoginController in '..\Controller\LoginController.pas',
  MainController in '..\Controller\MainController.pas',
  UsersController in '..\Controller\UsersController.pas',
  XiaoShouController in '..\Controller\XiaoShouController.pas',
  Plugin.Tool in '..\Plugin\Plugin.Tool.pas';

{$R *.res}

begin
  Config.password_key := '';   //配置文件解密秘钥
  _MVCFun.Run();
end.

