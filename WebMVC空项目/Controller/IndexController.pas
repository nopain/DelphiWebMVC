unit IndexController;

interface

uses
  MVC.BaseController, System.SysUtils, System.Classes, XSuperObject, uTableMap;

type
  TIndexController = class(TBaseController)
    procedure Index;
  end;

implementation



{ TIndexController }

procedure TIndexController.Index;
begin
  with View do
  begin
    ShowHTML('index');
  end;
end;

end.
