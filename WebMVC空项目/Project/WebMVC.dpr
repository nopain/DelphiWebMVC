{*******************************************************}
{                                                       }
{       苏兴迎                                          }
{       E-Mail:pearroom@yeah.net                        }
{       管理员权限启动delphi,管理员权限启动部署程序     }
{                                                       }
{*******************************************************}

program WebMVC;
{$APPTYPE GUI}
//{$APPTYPE CONSOLE}
uses
  MVC.Command,
  MVC.Config,
  uDBConfig in '..\Config\uDBConfig.pas',
  uGlobal in '..\Config\uGlobal.pas',
  uInterceptor in '..\Config\uInterceptor.pas',
  uPlugin in '..\Config\uPlugin.pas',
  uRouleMap in '..\Config\uRouleMap.pas',
  uTableMap in '..\Config\uTableMap.pas',
  Plugin.Tool in '..\Plugin\Plugin.Tool.pas',
  IndexController in '..\Controller\IndexController.pas';

{$R *.res}
begin
  Config.password_key := '';   //配置文件解密秘钥
  _MVCFun.Run();
end.

